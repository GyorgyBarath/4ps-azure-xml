pageextension 50200 MyExtension extends "Project Purchase Quote List"
{
    layout
    {
        // Add changes to page layout here
    }

    actions
    {
        addlast(Creation)
        {

            action(New2)
            {
                Caption = 'Create from Current Project2';
                Promoted = true;
                PromotedCategory = New;
                PromotedIsBig = true;
                Image = New;
                ApplicationArea = All;

                trigger OnAction()
                var
                    PurchaseHeader: Record "Purchase Header";
                    PurchaseQuote: Page "Purchase Quote";
                    JobL: Record Job;
                begin
                    PurchaseHeader.Init;
                    PurchaseHeader.SetRange("Document Type", PurchaseHeader."Document Type"::Quote);
                    if Rec.GetFilter("Job No.") <> '' then begin
                        PurchaseHeader.SetRange("Job No.", GetFilter("Job No."));
                        PurchaseHeader.Validate("Job No.", GetFilter("Job No."));
                    end;
                    if JobL.Get(GetFilter("Job No.")) then
                        PurchaseHeader."Job Description" := JobL.Description;
                    PurchaseQuote.SetTableView(PurchaseHeader);
                    PurchaseQuote.SetRecord(PurchaseHeader);
                    PurchaseQuote.Run();
                end;
            }
        }
    }

    var
        myInt: Integer;
}