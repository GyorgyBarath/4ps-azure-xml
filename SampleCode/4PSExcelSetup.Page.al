page 50200 "4PS Excel Setup"
{
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = "4PS Excel Setup";

    layout
    {
        area(Content)
        {
            group(GroupName)
            {
                ShowCaption = false;
                field(Name; "Base Management Url")
                {
                    ApplicationArea = All;
                }
                field("Login Name/E-mail"; "Login Name/E-mail")
                {
                    ApplicationArea = All;
                }
                field(Password; Password)
                {
                    ApplicationArea = All;
                }
                field("Automatic Storage Type"; "Automatic Storage Type")
                {
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action("Create New Excel")
            {
                ApplicationArea = All;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = NewDocument;

                trigger OnAction()
                var
                    PSExcelManagement: Codeunit "4PS Excel Management";
                    ExcelFileInStream: InStream;
                begin
                    PSExcelManagement.CreateNewExcel(ExcelFileInStream, 'GyuriTestSheet');
                    PSExcelManagement.SaveExcelFile(ExcelFileInStream, 'GyuriTestFile');
                    //PSExcelManagement.OpenExcelFile(ExcelFileInStream, 'C:\ProgramData\Microsoft\Microsoft Dynamics NAV\160\Server\MicrosoftDynamicsNavServer$BC160\users\default\METAPHORIX\GYORGYB\TEMP\GyuriTestFile.xlsx');
                    //PSExcelManagement.SaveExcelFile(ExcelFileInStream, 'GyuriTestFile2');
                end;
            }
            action("Create Excel From Json")
            {
                ApplicationArea = All;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = JobResponsibility;

                trigger OnAction()
                var
                    PSExcelManagement: Codeunit "4PS Excel Management";
                    JsonInStream: InStream;
                    ExcelFileInStream: InStream;
                begin
                    //PSExcelManagement.CreateExcelFromJsonData(JsonInStream, ExcelFileInStream, 'D:\Customer List 69 CRONUS_TEST 24052021112213-775_NEW.json');
                    PSExcelManagement.CreateExcelFromJsonData(JsonInStream, ExcelFileInStream, 'C:\Users\gyorgyb\Desktop\Excel JSON Format Version 2 CRONUS_TEST 03062021101547-521.json');
                    PSExcelManagement.SaveExcelFile(ExcelFileInStream, 'GyuriTestFile');
                end;
            }
            action("Deserializ Test Function")
            {
                ApplicationArea = All;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                Image = JobResponsibility;

                trigger OnAction()
                var
                    PSExcelManagement: Codeunit "4PS Excel Management";
                    JsonInStream: InStream;
                    ExcelFileInStream: InStream;
                begin
                    //PSExcelManagement.TriggerDeserializeTest(JsonInStream, 'D:\Customer List 69 CRONUS_TEST 18052021105753-408.json');
                    PSExcelManagement.TriggerDeserializeTest(JsonInStream, 'C:\Users\gyorgyb\Desktop\Excel JSON Format Version 2 CRONUS_TEST 03062021101547-521.json');
                end;
            }
            action(EraseChangeLogEntry)
            {
                ApplicationArea = All;

                trigger OnAction()
                var
                    ChangeLogEntry: Record "Change Log Entry";
                    Counter: Integer;
                begin
                    if ChangeLogEntry.FindSet() then
                        repeat
                            ChangeLogEntry.Delete();
                            Counter += 1;
                            if Counter = 5000 then begin
                                Counter := 0;
                                Commit();
                            end;
                        until ChangeLogEntry.Next() = 0
                end;
            }
        }
    }
}