table 50200 "4PS Excel Setup"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; PrimaryKey; Code[5])
        {
            DataClassification = SystemMetadata;
        }
        field(2; "Base Management Url"; Text[250])
        {
            DataClassification = CustomerContent;
        }
        field(3; "Login Name/E-mail"; Text[250])
        {
            DataClassification = CustomerContent;
        }
        field(4; Password; Text[250])
        {
            DataClassification = CustomerContent;
            ExtendedDatatype = Masked;
        }
        field(5; "Automatic Storage Type"; Option)
        {
            DataClassification = CustomerContent;
            OptionMembers = "Azure Blob Storage","One Drive/Sharepoint","Save as Server File","Save on Client";
        }

    }

    keys
    {
        key(Key1; PrimaryKey)
        {
            Clustered = true;
        }
    }

}