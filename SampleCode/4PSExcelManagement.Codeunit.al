codeunit 50200 "4PS Excel Management"
{
    procedure CreateNewExcel(var ExcelFileInStream: InStream; SheetName: Text)
    var
        Client: HttpClient;
        Content: HttpContent;
        ResponseMessage: HttpResponseMessage;
        Url: Text;
        PSExcelSetup: Record "4PS Excel Setup";
    begin
        PSExcelSetup.FindFirst();
        Url := PSExcelSetup."Base Management Url" + '/CreateExcel?&SheetName=' + SheetName;
        if not client.Post(Url, Content, ResponseMessage) then
            exit;
        if not ResponseMessage.IsSuccessStatusCode() then
            exit;
        ResponseMessage.Content().ReadAs(ExcelFileInStream);
    end;

    procedure SaveExcelFile(var ExcelFileInStream: InStream; FileName: Text)
    var
        OutStr: OutStream;
        FileManagement: Codeunit "File Management";
        TempBlob: Codeunit "Temp Blob";
    begin
        TempBlob.CreateOutStream(OutStr);
        CopyStream(OutStr, ExcelFileInStream);
        FileManagement.SaveStreamToFileServerFolder(TempBlob, FileName + '.xlsx', '.xlsx', '');
    end;

    procedure OpenExcelFile(var ExcelFileOutStream: OutStream; FileName: Text)
    var
        FileManagement: Codeunit "File Management";
        TempBlob: Codeunit "Temp Blob";
    begin
        TempBlob.CreateOutStream(ExcelFileOutStream);
        FileManagement.ReadFileContentToOutStream(FileName, ExcelFileOutStream);
    end;

    procedure CreateExcelFromJsonData(var JsonInStream: InStream; var ExcelFileInStream: InStream; FileName: Text)
    var
        Client: HttpClient;
        Content: HttpContent;
        ResponseMessage: HttpResponseMessage;
        Url: Text;
        PSExcelSetup: Record "4PS Excel Setup";
        TempBlob: Codeunit "Temp Blob";
        FileManagement: Codeunit "File Management";
    begin
        PSExcelSetup.FindFirst();
        TempBlob.CreateInStream(JsonInStream);
        FileManagement.BLOBImportFromServerFile(TempBlob, FileName);
        Content.WriteFrom(JsonInStream);
        Url := PSExcelSetup."Base Management Url" + '/CreateExcelFromJson?';
        Client.Post(Url, Content, ResponseMessage);
        ResponseMessage.Content.ReadAs(ExcelFileInStream);
    end;

    procedure TriggerDeserializeTest(var JsonInStream: InStream; FileName: Text)
    var
        Client: HttpClient;
        Content: HttpContent;
        Response: HttpResponseMessage;
        Result: Text;
        FileManagement: Codeunit "File Management";
        TempBlob: Codeunit "Temp Blob";
    begin
        TempBlob.CreateInStream(JsonInStream);
        FileManagement.BLOBImportFromServerFile(TempBlob, FileName);

        Content.WriteFrom(JsonInStream);
        Client.Post('https://4psclosedxmlgeneral.azurewebsites.net/api/DeserializeTest?', Content, Response);
        Response.Content.ReadAs(Result);
        Message(Result);
    end;

}