using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Collections.Generic;
using QuickType;
using ClosedXML.Excel;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using A = DocumentFormat.OpenXml.Drawing;
using Xdr = DocumentFormat.OpenXml.Drawing.Spreadsheet;
using A14 = DocumentFormat.OpenXml.Office2010.Drawing;
using C = DocumentFormat.OpenXml.Drawing.Charts;
using Color = DocumentFormat.OpenXml.Spreadsheet.Color;
using Font = DocumentFormat.OpenXml.Spreadsheet.Font;

namespace _4PSClosedXMLGeneral
{
    public static class Function1
    {
        [FunctionName("CreateExcel")]
        public static HttpResponseMessage CreateExcel([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequestMessage request, ILogger log)
        {
            var stream = request.Content.ReadAsStreamAsync().Result;
            byte[] outArray;
            var outStream = new MemoryStream();
            System.Collections.Specialized.NameValueCollection QueryString = request.RequestUri.ParseQueryString();
            String Sheetname = QueryString["SheetName"];
            using (var workbook = new ClosedXML.Excel.XLWorkbook())
            {
                if(Sheetname == "") 
                {
                    Sheetname = "Sheet1";
                }
                var worksheet = workbook.Worksheets.Add(Sheetname);
                worksheet.Cell("A1").Value = "Hello World!";
                worksheet.Cell("A2").FormulaA1 = "=MID(A1, 7, 5)";
                workbook.SaveAs(outStream);
                outArray = outStream.ToArray();
            }
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(outArray);
            result.Content.Headers.ContentType =new MediaTypeHeaderValue("application/json");
            return result;
        }
        [FunctionName("CreateExcelFromJson")]
        public static HttpResponseMessage CreateExcelFromJson([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req, ILogger log) 
        {
            String requestBody =  new StreamReader(req.Body).ReadToEnd();
            ExcelDocument DeserializedDocument = JsonConvert.DeserializeObject<ExcelDocument>(requestBody);


            //String Sheetname = "Sheet1";
            byte[] outArray;
            var outStream = new MemoryStream();
            var workbook = new XLWorkbook();
            
                Dictionary<String,QuickType.Style> StyleList = new Dictionary<string, QuickType.Style>();
                foreach (QuickType.Style ExcellStyle in DeserializedDocument.Workbook.Styles)
                {
                    StyleList.Add(ExcellStyle.Id,ExcellStyle);
                }
                Int32 SheetCounter = 1;
                foreach (QuickType.Worksheet ExcellWorksheet in DeserializedDocument.Workbook.Worksheets)
                {
                    String SheetName = "";

                if (ExcellWorksheet.Charts != null)
                {
                    foreach (QuickType.Chart ExcelChart in ExcellWorksheet.Charts)
                    {
                        var openxmlStream = new MemoryStream();
                        int seriesCounter = 0;
                        workbook.SaveAs(openxmlStream);
                        Dictionary<String, double> ChartData = new Dictionary<string, double>();
                        foreach (string seriescaption in ExcellWorksheet.Charts[0].SeriesCaption)
                        {
                            ChartData.Add(seriescaption, ExcellWorksheet.Charts[0].DataSeries[0].Points[seriesCounter]);
                            seriesCounter += 1;
                        }
                        createDocument(0, 0, 15, 15, ChartData, ref openxmlStream, ExcellWorksheet.Name);
                        workbook = new XLWorkbook(openxmlStream);

                    }
                }
                else
                {

                    if ((ExcellWorksheet.Name != null) && (ExcellWorksheet.Name != ""))
                    { SheetName = ExcellWorksheet.Name; }
                    else { SheetName = "Sheet" + SheetCounter.ToString(); }
                    var worksheet = workbook.Worksheets.Add(SheetName);
                    SheetCounter += 1;
                    if ((DeserializedDocument.Workbook.Password != null)) { worksheet.Protect(DeserializedDocument.Workbook.Password); }
                    if (ExcellWorksheet.Cells != null)
                    {
                        if (ExcellWorksheet.Cells.Count > 0)
                        {
                            //worksheet.Columns().AdjustToContents();
                            foreach (QuickType.Cell ExcelCell in ExcellWorksheet.Cells)
                            {
                                worksheet.Cell(ExcelCell.Id).Value = ExcelCell.Data;
                                if (ExcelCell.StyleId != "")
                                {
                                    QuickType.Style BufferStyle = StyleList[ExcelCell.StyleId];
                                    IXLStyle IXLBufferStyle = XLWorkbook.DefaultStyle;
                                    IXLBufferStyle.Font.SetFontName(BufferStyle.FontName);
                                    IXLBufferStyle.Font.SetFontSize(BufferStyle.Size);
                                    if ((BufferStyle.VerticalAlignment != null) && (BufferStyle.VerticalAlignment != ""))
                                        if (Enum.IsDefined(typeof(XLAlignmentVerticalValues), BufferStyle.VerticalAlignment))
                                        {
                                            IXLBufferStyle.Alignment.SetVertical((XLAlignmentVerticalValues)Enum.Parse(typeof(XLAlignmentVerticalValues), BufferStyle.VerticalAlignment));
                                        }
                                    if ((BufferStyle.HorizontalAlignment != null) && (BufferStyle.HorizontalAlignment != ""))
                                        if (Enum.IsDefined(typeof(XLAlignmentHorizontalValues), BufferStyle.HorizontalAlignment))
                                        {
                                            IXLBufferStyle.Alignment.SetHorizontal((XLAlignmentHorizontalValues)Enum.Parse(typeof(XLAlignmentHorizontalValues), BufferStyle.HorizontalAlignment));
                                        }
                                    if (BufferStyle.WrapText == true) { IXLBufferStyle.Alignment.SetWrapText(true); }
                                    if (BufferStyle.Bold == true) { IXLBufferStyle.Font.SetBold(true); }
                                    if (BufferStyle.Protected == false) { IXLBufferStyle.Protection.Locked = false; }
                                    if ((BufferStyle.InteriorColor != "") && (BufferStyle.InteriorColor != null))
                                    {
                                        XLColor someColor = XLColor.FromHtml(BufferStyle.InteriorColor);
                                        IXLBufferStyle.Fill.SetBackgroundColor(someColor);
                                    }
                                    if ((BufferStyle.InteriorPattern != "") && (BufferStyle.InteriorPattern != null))
                                        if (Enum.IsDefined(typeof(XLFillPatternValues), BufferStyle.InteriorPattern))
                                        {
                                            XLFillPatternValues InteriorPatternTypeValue = (XLFillPatternValues)Enum.Parse(typeof(XLFillPatternValues), BufferStyle.InteriorPattern);
                                            IXLBufferStyle.Fill.SetPatternType(InteriorPatternTypeValue);
                                        }
                                    if ((BufferStyle.BottomBorderLineStyle != null) && (BufferStyle.BottomBorderLineStyle != ""))
                                    {
                                        if (Enum.IsDefined(typeof(XLBorderStyleValues), BufferStyle.BottomBorderLineStyle))
                                        {
                                            IXLBufferStyle.Border.BottomBorder = (XLBorderStyleValues)Enum.Parse(typeof(XLBorderStyleValues), BufferStyle.BottomBorderLineStyle);
                                        }
                                    }
                                    if ((BufferStyle.TopBorderLineStyle != null) && (BufferStyle.TopBorderLineStyle != ""))
                                    {
                                        if (Enum.IsDefined(typeof(XLBorderStyleValues), BufferStyle.TopBorderLineStyle))
                                        {
                                            IXLBufferStyle.Border.TopBorder = (XLBorderStyleValues)Enum.Parse(typeof(XLBorderStyleValues), BufferStyle.TopBorderLineStyle);
                                        }
                                    }
                                    if ((BufferStyle.NumberFormat != null) && ((BufferStyle.NumberFormat != "")))
                                    {
                                        IXLBufferStyle.NumberFormat.SetFormat(BufferStyle.NumberFormat);
                                    }

                                    worksheet.Cell(ExcelCell.Id).Style = IXLBufferStyle;
                                }
                            }
                            //foreach(Column ExcelCol in ExcellWorksheet.Columns)
                            //{
                            //worksheet.Column((int)ExcelCol.Id).Width = ExcelCol.Width;                        
                            //}
                            //worksheet.Column(1).Width = 265;

                        }

                    }
                }

                }
                workbook.SaveAs(outStream);
                outArray = outStream.ToArray();
            
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(outArray);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return result;
        }

        private static void createDocument(int startRowIndexP, int startColumnIndexP, int endRowIndexP, int endColumnIndexP, Dictionary<String, double> ChartData, ref MemoryStream openxmlStream, string plantNameP)
        {

            uint cnAxisId1 = 1;
            uint cnAxisId2 = 2;
            const int cnDataWidth = 1;
            int cnDataHeight = ChartData.Count;
            double[,] faChartData = new double[cnDataHeight, cnDataWidth];
            string[] saCategories = new string[cnDataWidth];
            string[] saLegend = new string[cnDataHeight];
            WorkbookPart workbookPart = null;
            //using (openxmlStream)
            //{
            using (var excel = SpreadsheetDocument.Open(openxmlStream, true))
            //using (var excel = SpreadsheetDocument.Create(openxmlStream, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook, true))
            {
                int dataCounter = 0;
                foreach (KeyValuePair<string, double> entry in ChartData)
                {
                    saLegend[dataCounter] = entry.Key;
                    faChartData[dataCounter, 0] = entry.Value;
                    dataCounter += 1;
                }

                workbookPart = excel.WorkbookPart;
                //workbookPart.Workbook = workbookPart.Workbook;
                //excel.WorkbookPart.Workbook.Sheets = new Sheets();
                Sheets sheets = excel.WorkbookPart.Workbook.GetFirstChild<Sheets>();

                //WorkbookStylesPart stylesPart = excel.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                //stylesPart.Stylesheet = GenerateStyleSheet();
                //stylesPart.Stylesheet.Save();

                string sheetName = plantNameP;

                string relationshipId = "rId666";
                WorksheetPart wSheetPart = workbookPart.AddNewPart<WorksheetPart>(relationshipId);
                Sheet sheet = new Sheet() { Id = relationshipId, SheetId = 1, Name = sheetName };
                sheets.Append(sheet);

                DocumentFormat.OpenXml.Spreadsheet.Worksheet worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet();
                wSheetPart.Worksheet = worksheet;

                SheetData sheetData = new SheetData();
                worksheet.Append(sheetData);


                Drawing drawing1 = new Drawing() { Id = relationshipId };
                worksheet.Append(drawing1);
                DrawingsPart drawingsPart1 = wSheetPart.AddNewPart<DrawingsPart>(relationshipId);
                GenerateDrawingsPart1Content(drawingsPart1);
                BuildChart(drawingsPart1, sheetName, startRowIndexP, startColumnIndexP, endRowIndexP, endColumnIndexP, cnDataWidth, cnDataHeight, saCategories, faChartData, saLegend, cnAxisId1, cnAxisId2);

                excel.Close();
            }

            //return (openxmlStream);
            //}

        }

        private static void BuildChart(DrawingsPart dp, string sheetName, int startRowIndex, int startColumnIndex, int endRowIndex, int endColumnIndex, int cnDataWidth, int cnDataHeight, string[] saCategories, double[,] faChartData, string[] saLegend, uint cnAxisId1, uint cnAxisId2)
        {

            string cnWorksheetName = sheetName;
            ChartPart chartp = dp.AddNewPart<ChartPart>();
            chartp.ChartSpace = new C.ChartSpace();

            C.Chart chart = new C.Chart();

            chart.PlotArea = new C.PlotArea();
            chart.PlotArea.Layout = new C.Layout();

            int i, j;
            C.BarChartSeries bcs;

            C.BarChart bc = new C.BarChart();
            bc.BarDirection = new C.BarDirection() { Val = C.BarDirectionValues.Bar };
            bc.BarGrouping = new C.BarGrouping() { Val = C.BarGroupingValues.Clustered };


            C.CategoryAxisData cad = new C.CategoryAxisData();
            cad.StringReference = new C.StringReference();
            cad.StringReference.StringCache = new C.StringCache();
            for (j = 0; j < cnDataWidth; ++j)
            {
                cad.StringReference.StringCache.Append(new C.StringPoint()
                {
                    Index = (uint)j,
                    NumericValue = new C.NumericValue(saCategories[j])
                });
            }
            cad.StringReference.StringCache.PointCount = new C.PointCount() { Val = (UInt32)cnDataWidth };



            C.Values vals;
            for (i = 0; i < cnDataHeight; ++i)
            {
                bcs = new C.BarChartSeries();
                bcs.Index = new C.Index() { Val = (uint)i };
                bcs.Order = new C.Order() { Val = (uint)i };

                bcs.SeriesText = new C.SeriesText();
                bcs.SeriesText.StringReference = new C.StringReference();
                bcs.SeriesText.StringReference.StringCache = new C.StringCache();
                bcs.SeriesText.StringReference.StringCache.PointCount = new C.PointCount() { Val = 1 };
                bcs.SeriesText.StringReference.StringCache.Append(new C.StringPoint()
                {
                    Index = 0,
                    NumericValue = new C.NumericValue(saLegend[i])
                });


                bcs.Append((C.CategoryAxisData)cad.CloneNode(true));

                vals = new C.Values();
                vals.NumberReference = new C.NumberReference();
                vals.NumberReference.NumberingCache = new C.NumberingCache();
                vals.NumberReference.NumberingCache.FormatCode = new C.FormatCode("General");
                for (j = 0; j < cnDataWidth; ++j)
                {
                    vals.NumberReference.NumberingCache.Append(new C.NumericPoint()
                    {
                        Index = (uint)j,
                        NumericValue = new C.NumericValue(faChartData[i, j].ToString())
                    });
                }
                vals.NumberReference.NumberingCache.PointCount = new C.PointCount() { Val = (UInt32)cnDataWidth };
                bcs.Append(vals);

                bc.Append(bcs);
            }

            bc.Append(new C.AxisId() { Val = cnAxisId1 });
            bc.Append(new C.AxisId() { Val = cnAxisId2 });
            bc.Append(new C.Overlap() { Val = -20 });
            bc.Append(new C.GapWidth() { Val = 115 });


            chart.PlotArea.Append(bc);

            C.CategoryAxis catax = new C.CategoryAxis();
            catax.AxisId = new C.AxisId() { Val = cnAxisId1 };
            catax.Scaling = new C.Scaling()
            {
                Orientation = new C.Orientation() { Val = C.OrientationValues.MinMax }
            };
            catax.AxisPosition = new C.AxisPosition() { Val = C.AxisPositionValues.Bottom };
            catax.TickLabelPosition = new C.TickLabelPosition() { Val = C.TickLabelPositionValues.NextTo };
            catax.CrossingAxis = new C.CrossingAxis() { Val = cnAxisId2 };
            catax.Append(new C.Crosses() { Val = C.CrossesValues.AutoZero });
            catax.Append(new C.AutoLabeled() { Val = true });
            catax.Append(new C.LabelAlignment() { Val = C.LabelAlignmentValues.Center });
            catax.Append(new C.LabelOffset() { Val = 100 });
            chart.PlotArea.Append(catax);

            C.ValueAxis valax = new C.ValueAxis();
            valax.AxisId = new C.AxisId() { Val = cnAxisId2 };
            valax.Scaling = new C.Scaling()
            {
                Orientation = new C.Orientation() { Val = C.OrientationValues.MinMax }
            };
            valax.AxisPosition = new C.AxisPosition() { Val = C.AxisPositionValues.Left };
            valax.MajorGridlines = new C.MajorGridlines();
            valax.NumberingFormat = new C.NumberingFormat()
            {
                FormatCode = "General",
                SourceLinked = true
            };
            valax.TickLabelPosition = new C.TickLabelPosition() { Val = C.TickLabelPositionValues.NextTo };
            valax.CrossingAxis = new C.CrossingAxis() { Val = cnAxisId1 };
            valax.Append(new C.Crosses() { Val = C.CrossesValues.AutoZero });
            valax.Append(new C.CrossBetween() { Val = C.CrossBetweenValues.Between });
            chart.PlotArea.Append(valax);

            chart.Legend = new C.Legend();
            chart.Legend.LegendPosition = new C.LegendPosition() { Val = C.LegendPositionValues.Right };
            chart.Legend.Append(new C.Layout());

            chart.PlotVisibleOnly = new C.PlotVisibleOnly() { Val = true };

            chartp.ChartSpace.Append(chart);

            // end of the chart content

            // The drawings part of the chart
            Xdr.GraphicFrame gf = new Xdr.GraphicFrame();
            gf.Macro = string.Empty;
            gf.NonVisualGraphicFrameProperties = new Xdr.NonVisualGraphicFrameProperties();
            gf.NonVisualGraphicFrameProperties.NonVisualDrawingProperties = new Xdr.NonVisualDrawingProperties();
            // this has to be unique within the WorksheetDrawing class of the DrawingsPart
            // Continue with a different ID for other charts and other images.
            // Yes, normal images too.
            gf.NonVisualGraphicFrameProperties.NonVisualDrawingProperties.Id = 2;
            // give a friendly name
            gf.NonVisualGraphicFrameProperties.NonVisualDrawingProperties.Name = "Chart 1";
            gf.NonVisualGraphicFrameProperties.NonVisualGraphicFrameDrawingProperties = new Xdr.NonVisualGraphicFrameDrawingProperties();

            gf.Transform = new Xdr.Transform();
            gf.Transform.Offset = new A.Offset() { X = 0, Y = 0 };
            gf.Transform.Extents = new A.Extents() { Cx = 0, Cy = 0 };


            gf.Graphic = new A.Graphic();
            gf.Graphic.GraphicData = new A.GraphicData();
            gf.Graphic.GraphicData.Uri = "http://schemas.openxmlformats.org/drawingml/2006/chart";
            gf.Graphic.GraphicData.Append(new C.ChartReference() { Id = dp.GetIdOfPart(chartp) });

            Xdr.TwoCellAnchor tcanchor = new Xdr.TwoCellAnchor();
            tcanchor.FromMarker = new Xdr.FromMarker();
            tcanchor.FromMarker.RowId = new Xdr.RowId(startRowIndex.ToString());
            // no offset
            tcanchor.FromMarker.RowOffset = new Xdr.RowOffset("0");
            tcanchor.FromMarker.ColumnId = new Xdr.ColumnId(startColumnIndex.ToString());
            // no offset
            tcanchor.FromMarker.ColumnOffset = new Xdr.ColumnOffset("0");

            tcanchor.ToMarker = new Xdr.ToMarker();
            tcanchor.ToMarker.RowId = new Xdr.RowId(endRowIndex.ToString());
            // no offset
            tcanchor.ToMarker.RowOffset = new Xdr.RowOffset("0");
            tcanchor.ToMarker.ColumnId = new Xdr.ColumnId(endColumnIndex.ToString());
            // no offset
            tcanchor.ToMarker.ColumnOffset = new Xdr.ColumnOffset("0");

            tcanchor.Append(gf);
            tcanchor.Append(new Xdr.ClientData());

            dp.WorksheetDrawing.Append(tcanchor);
            dp.WorksheetDrawing.Save();
        }

        private static void GenerateDrawingsPart1Content(DrawingsPart drawingsPart1)
        {
            Xdr.WorksheetDrawing worksheetDrawing1 = new Xdr.WorksheetDrawing();
            worksheetDrawing1.AddNamespaceDeclaration("xdr", "http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing");
            worksheetDrawing1.AddNamespaceDeclaration("a", "http://schemas.openxmlformats.org/drawingml/2006/main");

            Xdr.TwoCellAnchor twoCellAnchor1 = new Xdr.TwoCellAnchor() { EditAs = Xdr.EditAsValues.OneCell };
            Xdr.NonVisualDrawingProperties nonVisualDrawingProperties1 = new Xdr.NonVisualDrawingProperties() { Id = (UInt32Value)2U, Name = "Picture 1" };

            Xdr.NonVisualPictureDrawingProperties nonVisualPictureDrawingProperties1 = new Xdr.NonVisualPictureDrawingProperties();
            A.PictureLocks pictureLocks1 = new A.PictureLocks() { NoChangeAspect = true };

            nonVisualPictureDrawingProperties1.Append(pictureLocks1);

            A.BlipExtension blipExtension1 = new A.BlipExtension() { Uri = "{28A0092B-C50C-407E-A947-70E740481C1C}" };

            A14.UseLocalDpi useLocalDpi1 = new A14.UseLocalDpi() { Val = false };
            useLocalDpi1.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");

            blipExtension1.Append(useLocalDpi1);

            A.Transform2D transform2D1 = new A.Transform2D();
            A.Offset offset1 = new A.Offset() { X = 0L, Y = 0L };// { X = 1257300L, Y = 762000L };
            A.Extents extents1 = new A.Extents() { Cx = 2381250L, Cy = 628650L };

            transform2D1.Append(offset1);
            transform2D1.Append(extents1);

            A.PresetGeometry presetGeometry1 = new A.PresetGeometry() { Preset = A.ShapeTypeValues.Rectangle };
            A.AdjustValueList adjustValueList1 = new A.AdjustValueList();

            presetGeometry1.Append(adjustValueList1);

            drawingsPart1.WorksheetDrawing = worksheetDrawing1;
        }

    }
}




// <auto-generated />
//
// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using QuickType;
//
//    var excelDocument = ExcelDocument.FromJson(jsonString);

namespace QuickType
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class ExcelDocument
    {
        [JsonProperty("Workbook")]
        public Workbook Workbook { get; set; }
    }

    public partial class Workbook
    {
        [JsonProperty("Styles")]
        public List<Style> Styles { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("Worksheets")]
        public List<Worksheet> Worksheets { get; set; }
    }

    public partial class Style
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("FontName")]
        public string FontName { get; set; }

        [JsonProperty("Size")]
        public long Size { get; set; }

        [JsonProperty("VerticalAlignment", NullValueHandling = NullValueHandling.Ignore)]
        public string VerticalAlignment { get; set; }

        [JsonProperty("WrapText", NullValueHandling = NullValueHandling.Ignore)]
        public bool? WrapText { get; set; }

        [JsonProperty("Bold", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Bold { get; set; }

        [JsonProperty("InteriorColor", NullValueHandling = NullValueHandling.Ignore)]
        public string InteriorColor { get; set; }

        [JsonProperty("InteriorPattern", NullValueHandling = NullValueHandling.Ignore)]
        public string InteriorPattern { get; set; }

        [JsonProperty("NumberFormat", NullValueHandling = NullValueHandling.Ignore)]
        public string NumberFormat { get; set; }

        [JsonProperty("BottomBorderLineStyle", NullValueHandling = NullValueHandling.Ignore)]
        public string BottomBorderLineStyle { get; set; }

        [JsonProperty("BottomBorderWeight", NullValueHandling = NullValueHandling.Ignore)]
        public long? BottomBorderWeight { get; set; }

        [JsonProperty("HorizontalAlignment", NullValueHandling = NullValueHandling.Ignore)]
        public string HorizontalAlignment { get; set; }

        [JsonProperty("Protected", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Protected { get; set; }

        [JsonProperty("TopBorderLineStyle", NullValueHandling = NullValueHandling.Ignore)]
        public string TopBorderLineStyle { get; set; }

        [JsonProperty("TopBorderWeight", NullValueHandling = NullValueHandling.Ignore)]
        public long? TopBorderWeight { get; set; }
    }

    public partial class Worksheet
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Columns", NullValueHandling = NullValueHandling.Ignore)]
        public List<Column> Columns { get; set; }

        [JsonProperty("Rows", NullValueHandling = NullValueHandling.Ignore)]
        public List<Row> Rows { get; set; }

        [JsonProperty("Cells", NullValueHandling = NullValueHandling.Ignore)]
        public List<Cell> Cells { get; set; }

        [JsonProperty("DataValidation", NullValueHandling = NullValueHandling.Ignore)]
        public List<DataValidation> DataValidation { get; set; }

        [JsonProperty("ConditionalFormat", NullValueHandling = NullValueHandling.Ignore)]
        public List<ConditionalFormat> ConditionalFormat { get; set; }

        [JsonProperty("PageSetup")]
        public PageSetup PageSetup { get; set; }

        [JsonProperty("FittoPage", NullValueHandling = NullValueHandling.Ignore)]
        public bool? FittoPage { get; set; }

        [JsonProperty("RowstoRepeat")]
        public long RowstoRepeat { get; set; }

        [JsonProperty("Charts", NullValueHandling = NullValueHandling.Ignore)]
        public List<Chart> Charts { get; set; }
    }

    public partial class Cell
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("StyleId")]
        public string StyleId { get; set; }

        [JsonProperty("DataType")]
        public DataType DataType { get; set; }

        [JsonProperty("Data")]
        public string Data { get; set; }

        [JsonProperty("NamedCell", NullValueHandling = NullValueHandling.Ignore)]
        public string NamedCell { get; set; }

        [JsonProperty("MergeAcross", NullValueHandling = NullValueHandling.Ignore)]
        public long? MergeAcross { get; set; }

        [JsonProperty("Formula", NullValueHandling = NullValueHandling.Ignore)]
        public string Formula { get; set; }
    }

    public partial class Chart
    {
        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Gallery")]
        public long Gallery { get; set; }

        [JsonProperty("Format")]
        public long Format { get; set; }

        [JsonProperty("Width")]
        public long Width { get; set; }

        [JsonProperty("Height")]
        public long Height { get; set; }

        [JsonProperty("YCaption")]
        public string YCaption { get; set; }

        [JsonProperty("XCaption")]
        public string XCaption { get; set; }

        [JsonProperty("NumberFormat")]
        public string NumberFormat { get; set; }

        [JsonProperty("SeriesCaption")]
        public List<string> SeriesCaption { get; set; }

        [JsonProperty("DataSeries")]
        public List<DataSery> DataSeries { get; set; }
    }

    public partial class DataSery
    {
        [JsonProperty("No")]
        public long No { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Points")]
        public List<long> Points { get; set; }
    }

    public partial class Column
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("Width")]
        public double Width { get; set; }

        [JsonProperty("AutoFit", NullValueHandling = NullValueHandling.Ignore)]
        public bool? AutoFit { get; set; }
    }

    public partial class ConditionalFormat
    {
        [JsonProperty("Range")]
        public string Range { get; set; }

        [JsonProperty("Qualifier")]
        public string Qualifier { get; set; }

        [JsonProperty("Value1")]
        public long Value1 { get; set; }

        [JsonProperty("Style")]
        public string Style { get; set; }
    }

    public partial class DataValidation
    {
        [JsonProperty("Range")]
        public string Range { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("ErrorMessage")]
        public string ErrorMessage { get; set; }

        [JsonProperty("Min")]
        public long Min { get; set; }

        [JsonProperty("Max")]
        public long Max { get; set; }
    }

    public partial class PageSetup
    {
        [JsonProperty("BottomMargin", NullValueHandling = NullValueHandling.Ignore)]
        public double? BottomMargin { get; set; }

        [JsonProperty("LeftMargin", NullValueHandling = NullValueHandling.Ignore)]
        public double? LeftMargin { get; set; }

        [JsonProperty("RightMargin", NullValueHandling = NullValueHandling.Ignore)]
        public double? RightMargin { get; set; }

        [JsonProperty("TopMargin", NullValueHandling = NullValueHandling.Ignore)]
        public double? TopMargin { get; set; }

        [JsonProperty("HeaderHeight", NullValueHandling = NullValueHandling.Ignore)]
        public double? HeaderHeight { get; set; }

        [JsonProperty("HeaderData")]
        public string HeaderData { get; set; }

        [JsonProperty("FooterHeight", NullValueHandling = NullValueHandling.Ignore)]
        public double? FooterHeight { get; set; }

        [JsonProperty("FooterData")]
        public string FooterData { get; set; }
    }

    public partial class Row
    {
        [JsonProperty("Id")]
        public long Id { get; set; }

        [JsonProperty("StyleId")]
        public string StyleId { get; set; }

        [JsonProperty("Height")]
        public long Height { get; set; }
    }

    public enum DataType { Number, String };

    public partial class ExcelDocument
    {
        public static ExcelDocument FromJson(string json) => JsonConvert.DeserializeObject<ExcelDocument>(json, QuickType.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this ExcelDocument self) => JsonConvert.SerializeObject(self, QuickType.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                DataTypeConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class DataTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(DataType) || t == typeof(DataType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "Number":
                    return DataType.Number;
                case "String":
                    return DataType.String;
            }
            throw new Exception("Cannot unmarshal type DataType");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (DataType)untypedValue;
            switch (value)
            {
                case DataType.Number:
                    serializer.Serialize(writer, "Number");
                    return;
                case DataType.String:
                    serializer.Serialize(writer, "String");
                    return;
            }
            throw new Exception("Cannot marshal type DataType");
        }

        public static readonly DataTypeConverter Singleton = new DataTypeConverter();
    }
}
